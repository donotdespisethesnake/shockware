/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <avr/io.h> 
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "dev_timer.h"
#include "dev_gpio.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( (a) / clockCyclesPerMicrosecond() )
#define microsecondsToClockCycles(a) ( (a) * clockCyclesPerMicrosecond() )

// the prescaler is set so that timer0 ticks every 64 clock cycles, and the
// the overflow handler is called every 256 ticks.
#define MICROSECONDS_PER_TIMER0_OVERFLOW (clockCyclesToMicroseconds(64 * 256))

// the whole number of milliseconds per timer0 overflow
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)

// the fractional number of milliseconds per timer0 overflow. we shift right
// by three to fit these numbers into a byte. (for the clock speeds we care
// about - 8 and 16 MHz - this doesn't lose precision.)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define FRACT_MAX (1000 >> 3)

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

/* Note: timer 0 is set up to provide about 1000 ticks /sec
  = 16MHz/64/256 = 976.5625
*/

timer_callback_t timer0_callback;

timer_callback_t timer1_callback;

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

volatile unsigned long timer0_overflow_count = 0;
volatile unsigned long timer0_millis = 0;
static unsigned char timer0_fract = 0;

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

unsigned long millis(void)
{
	uint32_t temp;

	// disable interrupts while we read timer0_millis or we might get an
	// inconsistent value (e.g. in the middle of a write to timer0_millis)
	ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
	{
		temp= timer0_millis;
	}
	return temp;
}


void init_timer (uint8_t timer_no, timer_callback_t timer_cb, uint16_t freq)
{
	switch (timer_no)
	{
	case 0:
		// set timer 0 prescale factor to 64
		#if defined(__AVR_ATmega128__)
			// CPU specific: different values for the ATmega128
			sbi(TCCR0, CS02);
		#elif defined(TCCR0) && defined(CS01) && defined(CS00)
			// this combination is for the standard atmega8
			sbi(TCCR0, CS01);
			sbi(TCCR0, CS00);
		#elif defined(TCCR0B) && defined(CS01) && defined(CS00)
			// this combination is for the standard 168/328/1280/2560
			sbi(TCCR0B, CS01);
			sbi(TCCR0B, CS00);
		#elif defined(TCCR0A) && defined(CS01) && defined(CS00)
			// this combination is for the __AVR_ATmega645__ series
			sbi(TCCR0A, CS01);
			sbi(TCCR0A, CS00);
		#else
			#error Timer 0 prescale factor 64 not set correctly
		#endif

		// enable timer 0 overflow interrupt
		#if defined(TIMSK) && defined(TOIE0)
			sbi(TIMSK, TOIE0);
		#elif defined(TIMSK0) && defined(TOIE0)
			sbi(TIMSK0, TOIE0);
		#else
			#error	Timer 0 overflow interrupt not set correctly
		#endif

		timer0_callback = timer_cb;

		break;


	case 1:
		// setup timer 1, overflow

		// prescaler = 64
		TCCR1B |= (0<<CS12) | (1<<CS11) | (1<<CS10);

		timer1_callback = timer_cb;

		TIMSK |= (1 << TOIE1);
		break;

	case 10:
		// setup timer 1, CTC mode

		TCCR1A |= (0<<WGM11) | (0<<WGM10);

		// CTC mode, prescaler = 64
		TCCR1B |= (0<<WGM13) | (1<<WGM12) | (0<<CS12) | (1<<CS11) | (1<<CS10);

		uint32_t timer_freq = F_CPU / 64;
		OCR1A = (timer_freq/freq)-1;

		TIMSK |= (1 << OCIE1A);
		break;
	}
}


void set_timer_frequency (uint8_t timer_no, uint16_t freq)
{
	switch (timer_no)
	{
		case 1:
		{
			uint32_t timer_freq = F_CPU / 64;

			OCR1A = (timer_freq/freq)-1;
		}
		break;
	}
}

#if defined(__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
ISR(TIM0_OVF_vect)
#else
ISR(TIMER0_OVF_vect)
#endif
{
	// copy these to local variables so they can be stored in registers
	// (volatile variables must be read from memory on every access)
	unsigned long m = timer0_millis;
	unsigned char f = timer0_fract;

	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
	}

	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
	 
	if (timer0_callback)
		timer0_callback();
}

ISR(TIMER1_OVF_vect)
{
	if (timer1_callback)
		timer1_callback();
}
// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

