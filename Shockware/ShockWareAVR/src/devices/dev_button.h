/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */
 
#ifndef _DEV_BUTTON_H
#define _DEV_BUTTON_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------

#define btn_is_down(btn) (buttons_down & btn) 

#define btn_pressed(btn) (buttons_pressed & btn) 

//#define btn_event_down	btn_pressed (btn_down)
//#define btn_event_up	btn_pressed (btn_up)
//#define btn_event_menu	btn_pressed (btn_menu)
//#define btn_event_ok	btn_pressed (btn_ok)


// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// note, these are bitmasks
typedef enum {
	btn_none,
	btn_menu = 1,
	btn_ok   = 2,
	btn_down = 4,
	btn_up   = 8
} tButton;

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

extern uint8_t buttons_pressed;

extern uint8_t buttons_down;

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

bool get_buttons (void);


// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _DEV_BUTTON_H

