/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <dev_button.h>

#include "dev_timer.h"
#include "dev_gpio.h"
#include "def_board.h"
// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

#define DEBOUNCE_DELAY	50 //ms


// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

uint8_t buttons_down;
uint8_t buttons_pressed;

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

static uint8_t last_buttons_down;

static uint32_t last_debounce_time;

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

bool get_buttons (void)
{
	uint8_t buttons;

	pinMode (BTN_OK, DIR_INPUT_PULLUP);
	pinMode (BTN_MENU, DIR_INPUT_PULLUP);
	pinMode (BTN_DOWN_LEFT, DIR_INPUT_PULLUP);
	pinMode (BTN_UP_RIGHT, DIR_INPUT_PULLUP);

	clear_pin (LCD_RW);

	set_pin (LCD_MUX_BTN_LCD_N);
	
	buttons = 0;
	// active low
	if (digitalRead (BTN_OK)==0)
		buttons |= btn_ok;

	if (digitalRead (BTN_MENU)==0)
		buttons |= btn_menu;

	if(digitalRead (BTN_DOWN_LEFT)==0)
		buttons |= btn_down;

	if(digitalRead (BTN_UP_RIGHT)==0)
		buttons |= btn_up;

	clear_pin (LCD_MUX_BTN_LCD_N);

	pinMode (BTN_OK, DIR_OUTPUT);
	pinMode (BTN_MENU, DIR_OUTPUT);
	pinMode (BTN_DOWN_LEFT, DIR_OUTPUT);
	pinMode (BTN_UP_RIGHT, DIR_OUTPUT);

	// perform debouncing

	//
	if (buttons != last_buttons_down)
	{
		last_debounce_time = millis();
	}

	if (millis()-last_debounce_time > DEBOUNCE_DELAY)
	{
		if (buttons != buttons_down)
		{
			buttons_pressed = buttons_pressed | (buttons & ~buttons_down);
			buttons_down = buttons;
		}
	}

	last_buttons_down = buttons;

	return buttons_pressed;
}


/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

