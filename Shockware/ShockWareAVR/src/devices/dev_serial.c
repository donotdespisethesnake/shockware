/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdio.h>
#include <avr/io.h>

#include "dev_serial.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */


void serial_init(void)
{

}

void serial_init_port (uint8_t port_num, unsigned long baud_rate)
{
	unsigned long prescaler;

	UCSRB |= (1 << RXEN) | (1 << TXEN);						/* Turn on transmission and reception */
	UCSRC |= (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1);	/* Use 8-bit char size */

	prescaler = (unsigned long)F_CPU / 16ul / baud_rate  - 1;

	UBRRH = prescaler >> 8;		/* Load upper 8-bits*/
	UBRRL = prescaler;			/* Load lower 8-bits of the baud rate */
}

void serial_send_ch (uint8_t port_num, uint8_t ch)
{
	while (! (UCSRA & (1<<UDRE)))
	{
		/* Wait for empty transmit buffer*/
	}	
	UDR = ch ;
}

char serial_receive_ch (uint8_t port_num) 
{
	while ((UCSRA & (1 << RXC)) == 0)
	{
		/* Wait till data is received */
	}
	return UDR;
}

void serial_send_str (uint8_t port_num, char *str)
{
	while (*str)
	{
		serial_send_ch(port_num, *str);
		str++;
	}
}

static char buf[6];

void serial_send_int (uint8_t port_num, uint16_t val)
{
	sprintf (buf, "%d\n", val);
	serial_send_str (port_num, buf);
}

