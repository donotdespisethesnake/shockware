/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

//#include <asf.h>
#include <avr/io.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>

#include "dev_gpio.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

#define NOT_A_PIN 0
#define NOT_A_PORT 0

// these arrays map port names (e.g. port B) to the
// appropriate addresses for various functions (e.g. reading
// and writing)
const uint16_t PROGMEM port_to_mode_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &DDRB,
	(uint16_t) &DDRC,
	(uint16_t) &DDRD
};

const uint16_t PROGMEM port_to_output_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &PORTB,
	(uint16_t) &PORTC,
	(uint16_t) &PORTD
};

const uint16_t PROGMEM port_to_input_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &PINB,
	(uint16_t) &PINC,
	(uint16_t) &PIND
};

#define portOutputRegister(P) ( (volatile uint8_t *)( pgm_read_word( port_to_output_PGM + (P))) )
#define portInputRegister(P)  ( (volatile uint8_t *)( pgm_read_word( port_to_input_PGM + (P))) )
#define portModeRegister(P)   ( (volatile uint8_t *)( pgm_read_word( port_to_mode_PGM + (P))) )

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

 void pinMode (uint8_t pin, uint8_t dir)
 {
 #if 0
	volatile uint8_t *mode, *out;

	mode = portModeRegister(GET_PORT_NUM(pin));
	out = portOutputRegister(GET_PORT_NUM(pin));

	if (dir == DIR_INPUT)
	{
		ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
		{
			*mode &= ~ _BV(GET_BIT_NUM(pin));
			*out &= ~ _BV(GET_BIT_NUM(pin));
		}
	}
	else if (dir == DIR_INPUT_PULLUP)
	{
		ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
		{
			*mode &= ~ _BV(GET_BIT_NUM(pin));
			*out |=  _BV(GET_BIT_NUM(pin));
		}
	}
	else
	{
		ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
		{
			*out &= ~ _BV(GET_BIT_NUM(pin));
			*mode |= _BV(GET_BIT_NUM(pin));
		}
	}


#else
	ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
	 switch (GET_PORT_NUM(pin))
	 {
		 case PORT_B:
		 {
			 if (dir <= DIR_OUTPUT_HIGH)
				DDRB |= _BV(GET_BIT_NUM(pin));
			 else
				DDRB &= ~_BV(GET_BIT_NUM(pin));
		 }
		 break;

		 case PORT_C:
		 {
			 if (dir <= DIR_OUTPUT_HIGH)
				DDRC |= _BV(GET_BIT_NUM(pin));
			 else
				DDRC &= ~_BV(GET_BIT_NUM(pin));
		 }
		 break;

		 case PORT_D:
		 {
			 if (dir <= DIR_OUTPUT_HIGH)
				DDRD |= _BV(GET_BIT_NUM(pin));
			 else
				DDRD &= ~_BV(GET_BIT_NUM(pin));
		 }
		 break;
	 }
#endif

 }

 void set_pin (uint8_t pin)
 {
	ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
	 switch (GET_PORT_NUM(pin))
	 {
		 case PORT_B:
			PORTB |= _BV(GET_BIT_NUM(pin));
			break;
		 case PORT_C:
			PORTC |= _BV(GET_BIT_NUM(pin));
			break;
		 case PORT_D:
			PORTD |= _BV(GET_BIT_NUM(pin));
			break;
	 }
 }

 void clear_pin (uint8_t pin)
 {
	ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
	 switch (GET_PORT_NUM(pin))
	 {
		 case PORT_B:
			PORTB &= ~ _BV(GET_BIT_NUM(pin));
			break;
		 case PORT_C:
			PORTC &= ~ _BV(GET_BIT_NUM(pin));
			break;
		 case PORT_D:
			 PORTD &= ~ _BV(GET_BIT_NUM(pin));
			 break;
	 }
 }
 
 uint8_t digitalRead (uint8_t pin)
 {
	 switch (GET_PORT_NUM(pin))
	 {
		 case PORT_B:
			return PINB & _BV(GET_BIT_NUM(pin));
			break;
		 case PORT_C:
			return PINC & _BV(GET_BIT_NUM(pin));
			break;
		 case PORT_D:
			 return PIND & _BV(GET_BIT_NUM(pin));
			 break;
	 }
	 return 0;
 }

 void digitalWrite(uint8_t pin, uint8_t value)
 {
	 if (value)
		set_pin (pin);
	 else
		clear_pin(pin);
 }


 void toggle_pin(uint8_t pin)
 {
	volatile uint8_t *out;

	out = portOutputRegister(GET_PORT_NUM(pin));

	if (*out & _BV(GET_BIT_NUM(pin)))
		clear_pin( pin);
	else
		set_pin (pin);
 }
 
// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

