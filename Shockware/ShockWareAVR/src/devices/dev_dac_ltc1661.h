/*
 * Copyright (c) 2019 zaphod
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


/*
 * Description:
 *
 */

#ifndef _DEV_DAC_LTC1661_H
#define _DEV_DAC_LTC1661_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------

#define DAC_CHANNEL_A	0
#define DAC_CHANNEL_B	1


#define DAC_NOP					(0 << 12)
#define DAC_LOAD_A				(1 << 12)
#define DAC_LOAD_B				(2 << 12)
// reserved 3-7
#define DAC_UPDATE_OUTPUTS		(8 << 12)
#define DAC_LOAD_A_AND_UPDATE	(9 << 12)
#define DAC_LOAD_B_AND_UPDATE	(10 << 12)
//11-12 reserved
#define DAC_WAKE_UP             (13 << 12)
#define DAC_SLEEP               (14 << 12)
#define DAC_LOAD_BOTH_UPDATE    (15 << 12)

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

void dac_init(void);

/*	Write and update a single DAC channel
 *  channel : 0=channel A, 1=channel B
 *	val     : intensity level, 10 bits (1023 = max intensity)
 */

void dac_write_and_update (uint8_t channel, uint16_t val);

void dac_write_command (uint16_t command_word);

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _DEV_DAC_LTC1661_H

