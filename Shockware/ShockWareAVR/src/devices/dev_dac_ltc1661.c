/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description: Cmmand interface for LTC1661 DAC in MK312
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <avr/io.h>

#include "dev_gpio.h"
#include "dev_dac_ltc1661.h"

#include "def_board.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------



// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

void dac_init (void)
{
	 // Set MOSI, SCK as Output
	 pinMode (PIN_MOSI, DIR_OUTPUT);
	 pinMode (PIN_SCK, DIR_OUTPUT);

	 pinMode (PIN_SPI_CS, DIR_OUTPUT); set_pin (PIN_SPI_CS);
	 
	 // Enable SPI, Master mode
	 // Prescaler: Fosc/2 = 8 MHz/2 = 4 MHz
	 SPCR = (1<<SPE) | (1<<MSTR) | (0<<SPR1) | (0<<SPR0) | (0<<CPOL)| (0<<CPHA);
	 SPSR = (1<<SPI2X);
}


/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

void dac_write_command (uint16_t command)
{
	//set_pin(PIN_SS);

	CLEAR_PIN(SPI_CS);

	SPDR = command >> 8;
	// Wait until transmission complete
	while(!(SPSR & (1<<SPIF) ));

	SPDR = command & 0xFF;
		
	// Wait until transmission complete
	while(!(SPSR & (1<<SPIF) ));

	SET_PIN(SPI_CS);
}

void dac_write_and_update (uint8_t channel, uint16_t val)
{
	uint16_t dac_command;

// Note : the outputs are swapped in MK312 schematic, so that for LTC1661
// DAC channel A = box output B

	dac_command = DAC_LOAD_B;
	if (channel == DAC_CHANNEL_B)
		dac_command = DAC_LOAD_A;

	dac_command |= DAC_UPDATE_OUTPUTS;

	//make sure val is 10 bits, otherwise we could write garbage commands
	if (val > 1023)
		val = 1023;

	// invert the value to drive the FET 
	val = 1023-val;

	// left justify to 12 bits for DAC
	dac_command |= val << 2;

	dac_write_command (dac_command);
}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

