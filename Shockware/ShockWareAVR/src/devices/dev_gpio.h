/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */
 
#ifndef _DIGITAL_IO_H
#define _DIGITAL_IO_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdint.h>

#include <avr/io.h>

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------

#ifndef cbi
	#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
	#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#ifndef _BV
	#define _BV(bit) (1 << (bit))
#endif

#define PORT_A  0
#define PORT_B  1
#define PORT_C  2
#define PORT_D  3
#define PORT_E  4


#define DIR_OUTPUT			0
#define DIR_OUTPUT_LOW		1
#define DIR_OUTPUT_HIGH		2
#define DIR_INPUT			3
#define DIR_INPUT_PULLUP	4

#define LOW		0
#define HIGH	1

// port 0-63
// bit  0-7

typedef uint8_t pin_t;

#define PIN_SIZE 3
#define PIN_MASK 0x07

// encode port and bit number in a byte

#define DIGITAL_MAKE_PIN(port,bit) ((port)<<PIN_SIZE) | (bit)

// decode to port number and bitmask
#define GET_PORT_BITMASK(port_bit) ((port_bit)>>PIN_SIZE), 1<<((port_bit) & PIN_MASK)

// decode to port number
#define GET_PORT_NUM(port_bit) ((port_bit)>>PIN_SIZE)
// decode to bit number
#define GET_BIT_NUM(port_bit) ((port_bit) & PIN_MASK)


#define _SET_PIN(pin)    pin ## _WPORT |=  (1 << pin ## _PIN)
#define _CLEAR_PIN(pin)  pin ## _WPORT &=  ~(1 << pin ## _PIN)

#define _READ_PIN(pin)   (pin ## _RPORT &  1 << pin ## _PIN))

#define SET_PIN(pin) _SET_PIN(pin)
#define CLEAR_PIN(pin) _CLEAR_PIN(pin)

#define READ_PIN(pin) _READ_PIN(pin)

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

 void pinMode (uint8_t pin, uint8_t mode);

 void digitalWrite (uint8_t pin, uint8_t value);
 uint8_t digitalRead (uint8_t pin);

 void set_pin (uint8_t pin);
 void clear_pin (uint8_t pin);
 void toggle_pin (uint8_t pin);

 
// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _DIGITAL_IO_H

