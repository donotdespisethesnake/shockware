/*
 *
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

/*
 * Description:
 *
 */

/*
TODO:

check if on battery or not
- what are normal high/low battery values?

flash batt symbol on low battery
low battery error

DAC - enable update

self-test?

Bootloader?

serial protocol?

fastio

TO TEST:

test vin/batt voltages with PSU

BUGS:

save favorite also saves any changed advanced settings

timer glitch

DONE:

/debounce/eventize buttons
/average adc readings
/eeprom functions

*/


// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "dev_gpio.h"
#include "dev_lcd_char.h"
#include "dev_button.h"
#include "dev_timer.h"
#include "dev_analog.h"
#include "dev_serial.h"
#include "dev_delay.h"
#include "dev_eeprom.h"
#include "dev_dac_ltc1661.h"
#include "def_board.h"
#include "analog.h"
#include "task_modex.h"
#include "task_ui.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

typedef enum {
	menu_splash,
	menu_run,

	menu_start_ramp,
	menu_split,
	menu_set_favorite
} tMenu;


typedef struct {
	uint8_t min;
	uint8_t max;
	uint8_t _default;
} setting_range_t;

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// data that is always restored from EEPROM at startup
tSettings	settings;

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

tSettings factory_defaults = {
	.starting_mode	= 0, 

	.ramp_start		= 70,	// 
	.ramp_time		= 20,	// 
	.depth			= 50,
	.tempo			= 10,
	.frequency		= 150,
	.effect			= 5,
	.pulse_width	= 130,
	.pace			= 1,

	.split_mode_enabled = 0,
	.split_mode_a	= 1,	// stroke
	.split_mode_b	= 0,	// waves

	.power_level	= 1,

	.pause_at_powerup = 1,

	.checksum		= 0		// calculated on save
	};

static uint8_t batteryChar[] = {
  0x0E,		// B01110,
  0x11,		// B10001,
  0x11,		// B10001,
  0x11,		// B10001,
  0x11,		// B10001,
  0x11,		// B10001,
  0x11,		// B10001,
  0x1F,		// B11111
};

static uint8_t plugCharMap[] = {
	0x0A,		// 
	0x0A,		// 
	0x1F,		// 
	0x1F,		// 
	0x1F,		// 
	0x0E,		// 
	0x04,		// 
	0x04,		// 
};

static char plugChar = '\7';

// battery levels 0-6
/*
 * 0 empty
 * 1 1/6   17%
 * 2 2/6   29%
 * 3 half  50%
 * 4 4/6   67%
 * 5 5/6   83%
 * 6 full
 */

// current values, not saved
static tMenu		menu;

static uint32_t		timer_start;


// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

void lcd_print (uint8_t val, uint8_t digits)
{
	char buf [5];
	uint8_t j;

	if ((digits == 2) && (val>99))
		val = 99;

	j = 0;
	while (j < digits)
	{
		buf[digits-j-1] = val % 10 + '0';
		val = val / 10;
		j++;
	}

	buf[j] = 0;

	print (buf);
}

bool checksum_ok (void)
{
	int len = sizeof(settings);
	int sum = 0;
	int j;
	uint8_t *p = (uint8_t *)&settings;

	for (j=0; j < len-2; j++)
	sum += *p++;

	sum = sum ^ 0xFFFF;

	if (sum == settings.checksum)
		return true;
	else
		return false;
}

void load_eeprom_settings (void)
{
	EEPROM_get (0, settings);

	// debug
	setCursor(0, 0);
	if (checksum_ok())
	{
		print ("eeprom ok");
	}
	else
	{
		print ("eeprom nok");
		settings = factory_defaults;
	}
	delayMillisec (1000);

}

void save_eeprom_settings (void)
{
	int len = sizeof(settings);
	int sum = 0;
	int j;
	uint8_t *p = (uint8_t *)&settings;

	for (j=0; j < len-2; j++)
		sum += *p++;

	settings.checksum = sum ^ 0xFFFF;

	EEPROM_put (0, settings);
}

void start_timer (void)
{
	timer_start = millis();
}

bool timer_expired (uint32_t timeout)
{
	if (millis()-timer_start > timeout)
		return true;
	else 
		return false;
}

void display_screen(void)
{
	clear();
	switch (menu)
	{
		case menu_splash:
		setCursor(0, 0);
		print("Shockware v0.1");
		setCursor(0, 1);
		print("Press a key");
		start_timer();
		break;

		case menu_run:
		setCursor(0, 0);
		print("A:-- B:-- M:-- ");
		lcd_print_char ((char)0);
		
		setCursor(0, 1);
		print("??");

		setCursor(8, 1);
		print("Ramp:---");
#if 0
		// on change mode
		ramp_setting = settings.ramp_start;
		ramp_step = 1;
#endif
		break;

		case menu_start_ramp:
		setCursor(0, 0);
		print("Start ramp?");
		break;

		case menu_split:
		setCursor(0, 0);
		print("Split mode?");
		break;

		case menu_set_favorite:
		setCursor(0, 0);
		print("Set favorite?");
		break;
	}

}

void handle_events (void)
{
	uint8_t last_menu;
	bool b_pressed = get_buttons();

	last_menu = menu;

	{
		switch (menu)
		{
			case menu_splash:
				if (b_pressed || (!settings.pause_at_powerup && timer_expired(2000)) )
				{
					set_mode (settings.starting_mode);
					menu = menu_run;
				}
				break;

			case menu_run:
			{
				if (btn_pressed (btn_up))
				{
					cur_mode = (cur_mode +1) % num_modes;
					set_mode (cur_mode);
					//display_screen();
				}
				else if (btn_pressed (btn_down))
				{
					cur_mode--;
					if (cur_mode < 0)
						cur_mode = num_modes-1;
					set_mode (cur_mode);
					//display_screen();
				}
				else if (btn_pressed (btn_menu))
				{
					menu = menu_start_ramp;
				}
			}
			break;

			case menu_start_ramp:
			{
				if (btn_pressed (btn_up))
					menu = menu_split;
				else if (btn_pressed (btn_menu))
					menu = menu_run;
			}
			break;

			case menu_split:
			{
				if (btn_pressed (btn_up))
					menu = menu_set_favorite;
				else if (btn_pressed (btn_menu))
					menu = menu_run;
			}
			break;

			case menu_set_favorite:
			{
				if (btn_pressed (btn_up))
					menu = menu_start_ramp;

				else if (btn_pressed (btn_menu))
					menu = menu_run;

				else if (btn_pressed (btn_ok))
				{
					settings.starting_mode = cur_mode;
					save_eeprom_settings();

					setCursor(0,1);
					print ("Saved to EEPROM");
				}
			}
			break;
		}

		if (menu != last_menu)
		{
			display_screen();
		}

		buttons_pressed = 0;
	}
	
}

void update_screen (void)
{
	static uint8_t last_battery_level;

	uint16_t temp;

	switch (menu)
	{
		case menu_run:
		//           0123456789012345
		//lcd.print("A:99 B:99 M:99 x");
		setCursor(2, 0);
		lcd_print (ain_setting_a, 2);

		setCursor(7, 0);
		lcd_print (ain_setting_b, 2);

		setCursor(12, 0);
		lcd_print (ain_setting_f, 2);

//		if (battery_level != last_battery_level)

		setCursor(15, 0);
		if (ain_input_voltage == 0)
		{
			lcd_print_char ((char)battery_level);
			last_battery_level = battery_level;
		}
		else
			lcd_print_char (plugChar);

		setCursor(0, 1);
		print(mode_name [cur_mode]);
		for (temp=strlen(mode_name [cur_mode]); temp < 7; temp++)
			print (" ");

		setCursor(13, 1);
		temp = ramp_setting * 100 /255;
		lcd_print (temp, 3);
		break;

		case menu_splash:
		break;

		case menu_start_ramp:
		case menu_split:
		case menu_set_favorite:
		break;
	}
}

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

extern uint8_t buttons;

void task_ui_init(void)
{
	int j;

	begin (16, 2);

	createChar(0, batteryChar);
	for (j=1; j<7; j++)
	{
		batteryChar [7-j] = 0x1f;
		createChar(j, batteryChar);
	}

	createChar(plugChar, plugCharMap);

	load_eeprom_settings();

	// starts with splash screen
	display_screen();
}


void task_ui_poll(void)
{
	static uint32_t last_time=0;
	static uint32_t last_adc_poll=0;
	static bool state = false;
	static char buf [16];
	static uint8_t channel = 0;

	uint16_t adc;
	uint8_t j;


	if (millis()-last_time > 1000)
	{
		last_time = millis();
#if 0
		dump_analog_values();
#endif
	}

	// poll analog inputs at 50 Hz
	if (millis()-last_adc_poll > 20)
	{
		analog_poll();
		last_adc_poll = millis();

#if 0
	clear_pin (PIN_LED_B);
		//debug
		dac_write_and_update(DAC_CHANNEL_A, dac_val);
		dac_val ++ ;
		if (dac_val >= 256)
			dac_val = 0;
	set_pin (PIN_LED_B);
#endif
	}

	
#if 0
	// debug : show buttons
	//get_buttons();
	
	setCursor (0,1); 
	lcd_print (buttons_pressed, 1);

	if (buttons_pressed)
		serial_send_int(SERIAL_0, buttons_pressed);

#if 0
	setCursor (1,1); 
	if (btn_is_down(btn_ok))   serial_send_str (SERIAL_0, "OK ");
	if (btn_is_down(btn_menu)) serial_send_str (SERIAL_0, "MENU ");
	if (btn_is_down(btn_up))   serial_send_str (SERIAL_0, "UP ");
	if (btn_is_down(btn_down)) serial_send_str (SERIAL_0, "DOWN");
#endif
#endif

#if 0
	// debug : show ADC Value
	adc = read_adc (channel);
	setCursor (0,1); 
	sprintf (buf, "%d %04d", channel, adc);
	print (buf);
#endif

	handle_events();
	update_screen();


}

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

