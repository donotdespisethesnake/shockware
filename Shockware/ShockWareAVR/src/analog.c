/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

/*
 * Description:
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdint.h>
#include <stdio.h>

#include "def_board.h"
#include "analog.h"

#include "dev_analog.h"
#include "dev_serial.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

#define NUM_CHANNELS	8

// voltage x 100
#define MIN_BATTERY 1180
#define MAX_BATTERY 1270
// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

uint16_t adc_raw	  [NUM_CHANNELS];
uint16_t adc_filtered [NUM_CHANNELS];

uint16_t ain_output_current;	// 0-10 A
uint16_t ain_setting_f;			// percent
uint16_t ain_input_voltage;		// 0-20 V
uint16_t ain_batt_voltage;		// 0-20 V
uint16_t ain_setting_a;			// percent
uint16_t ain_setting_b;			// percent
uint16_t ain_audio_a;
uint16_t ain_audio_b;

// derived values
uint8_t  battery_level;			// 0..6
uint8_t  battery_percent;		// 0..100

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

uint16_t *real_values [NUM_CHANNELS] =
{
	&ain_output_current,
	&ain_setting_f,
	&ain_input_voltage,
	&ain_batt_voltage,
	&ain_setting_a,
	&ain_setting_b,
	&ain_audio_a,
	&ain_audio_b
};

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

 static uint16_t convert_adc (uint8_t channel)
 {
	 uint32_t val;

	 val = adc_filtered[channel];

	 /*
		adc voltage (V) = adc reading / 1024 * 5.0
	 */

	 switch (channel)
	 {
		 case ADC_OUTPUT_SENSE:
		 // current (Amps * 10)
		 // with sense resistor of 0.5R, I=V/R
		 // so current(A) = adc(v) * 2
		 val = val * 2 * 50 / 1024 ;
		 break;

		 case ADC_MULTI_ADJUST:
		 // 0..100
		 val = val * 100 / 341 ;
		 break;

		 case ADC_VIN:
		 // volts * 100
		 // gain of voltage divider = 120/20 = 6
		 val = val * 5;
		 val = val * 600 / 1024 ;
		 break;

		 case ADC_12V:
		 // volts * 100
		 // gain of voltage divider = 13.3/3.3 ~ 4.03
		 val = val * 5;
		 val = val * 403 / 1024 ;
		 break;

		 case ADC_LEVEL_A:
		 // 0..99
		 val = val * 100 / 1024;
		 break;

		 case ADC_LEVEL_B:
		 // 0..99
		 val = val * 100 / 1024;
		 break;

		 case ADC_AUDIO_A:
		 // 0..99
		 val = val * 100 / 1024;
		 break;

		 case ADC_AUDIO_B:
		 // 0..99
		 val = val * 100 / 1024;
		 break;

		 default:
		 // invalid channel?
		 break;
	 }
	 return val;
 }

static void calc_battery_level (void)
{
	if (ain_batt_voltage < MIN_BATTERY)
		battery_percent = 0;
	else if (ain_batt_voltage >= MAX_BATTERY)
		battery_percent = 100;
	else
		battery_percent = (ain_batt_voltage - MIN_BATTERY) * 100 / (MAX_BATTERY-MIN_BATTERY);

	 if (battery_percent >= 99)
		battery_level = 6; // full
	 else if (battery_percent < 1)
	 {
		 battery_level = 0;
	 }
	 else
		battery_level = battery_percent / 5 + 1;
}


// debug : output ADC readings
void dump_analog_values (void)
{
	uint8_t j;
	char buf [16];

	for (j=0; j < 8; j++)
	{
		sprintf (buf, "%d %4d %4d\n", j, adc_filtered[j], *real_values[j]);
		serial_send_str (SERIAL_0, buf);
	}

	sprintf (buf, "BATT %d, %d\n", battery_percent, battery_level);
	serial_send_str (SERIAL_0, buf);
}


void analog_poll (void)
{
	uint8_t channel;
	uint32_t temp;

	adc_raw [ADC_OUTPUT_SENSE]	= read_adc(ADC_OUTPUT_SENSE);
	adc_raw [ADC_MULTI_ADJUST]	= read_adc(ADC_MULTI_ADJUST);
	adc_raw [ADC_VIN]			= read_adc(ADC_VIN);
	adc_raw [ADC_12V]			= read_adc(ADC_12V);
	adc_raw [ADC_LEVEL_A]		= read_adc(ADC_LEVEL_A);
	adc_raw [ADC_LEVEL_B]		= read_adc(ADC_LEVEL_B);

	adc_raw [ADC_AUDIO_A]		= read_adc(ADC_AUDIO_A);
	adc_raw [ADC_AUDIO_B]		= read_adc(ADC_AUDIO_B);

	for (channel=0; channel < NUM_CHANNELS; channel++)
	{
		temp = adc_filtered [channel];
		temp = (temp + adc_raw [channel]) / 2;
		adc_filtered [channel] = temp;
	}

	// 
	ain_output_current	= convert_adc (ADC_OUTPUT_SENSE);
	ain_setting_a		= convert_adc (ADC_LEVEL_A);
	ain_setting_b		= convert_adc (ADC_LEVEL_B);
	ain_setting_f		= convert_adc (ADC_MULTI_ADJUST);
	ain_batt_voltage	= convert_adc (ADC_12V);
	ain_input_voltage	= convert_adc (ADC_VIN);

	calc_battery_level();
}
// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

