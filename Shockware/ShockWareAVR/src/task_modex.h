/*
 * Copyright (c) 2019 zaphod
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


/*
 * Description:
 *
 */

#ifndef _TASK_MODEX_H
#define _TASK_MODEX_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------


typedef enum {
	MODE_WAVES		= 0,	// yes
	MODE_STROKE		= 1,  // yes
	MODE_CLIMB		= 2,
	MODE_COMBO		= 3,
	MODE_INTENSE	= 4,  // yes
	MODE_RHYTHM		= 5,

	MODE_HIGHEST_ID = 15,
	MAX_MODES       = 16
} eModeId;

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

extern const char *mode_name [MAX_MODES];
extern uint8_t	cur_mode;
extern uint8_t	num_modes;

//
extern uint8_t	ramp_setting;
extern uint8_t	ramp_step;

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

void task_modex_init(void);
void task_modex_poll(void);

void modex_timer_cb (void);

void set_mode (uint8_t mode);

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _TASK_MODEX_H

