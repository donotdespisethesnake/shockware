/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

/*
 * Description:
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include "dev_gpio.h"
#include "dev_serial.h"
#include "dev_lcd_char.h"

#include "def_board.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

 void bdk_board_init (void)
 {
	// PORT B
  	pinMode (PIN_OUT_A0, DIR_OUTPUT); clear_pin(PIN_OUT_A0);
  	pinMode (PIN_OUT_A1, DIR_OUTPUT); clear_pin(PIN_OUT_A1);
  	pinMode (PIN_OUT_B0, DIR_OUTPUT); clear_pin(PIN_OUT_B0);
  	pinMode (PIN_OUT_B1, DIR_OUTPUT); clear_pin(PIN_OUT_B1);

  	pinMode (PIN_SS, DIR_OUTPUT);     set_pin(PIN_SS);

	// PORT C
 	pinMode (LCD_MUX_BTN_LCD_N, DIR_OUTPUT); clear_pin (LCD_MUX_BTN_LCD_N);

	// PORT D
 	pinMode (PIN_LED_A,		DIR_OUTPUT);
 	pinMode (PIN_LED_B,		DIR_OUTPUT);
 	pinMode (LCD_BL_N,	DIR_OUTPUT);

	// turn on backlight
 	clear_pin (LCD_BL_N);

 	serial_init ();
 	serial_init_port (SERIAL_0, 38400);

 	LiquidCrystal_4bit_rw (LCD_RS, LCD_RW, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

