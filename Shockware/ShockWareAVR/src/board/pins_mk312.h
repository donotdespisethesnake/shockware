/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */
 
#ifndef _PINS_MK312_H
#define _PINS_MK312_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include "dev_gpio.h"

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------

// ADC channels
#define ADC_OUTPUT_SENSE	0
#define ADC_MULTI_ADJUST	1
#define ADC_VIN				2
#define ADC_12V				3
#define ADC_LEVEL_A			4
#define ADC_LEVEL_B			5
#define ADC_AUDIO_B			6
#define ADC_AUDIO_A			7

// PORT B
#define PIN_OUT_B0		DIGITAL_MAKE_PIN(PORT_B, 0)
#define OUT_B0_WPORT	PORTB
#define OUT_B0_PIN		0

#define PIN_OUT_B1		DIGITAL_MAKE_PIN(PORT_B, 1)
#define OUT_B1_WPORT	PORTB
#define OUT_B1_PIN		1

#define PIN_OUT_A0		DIGITAL_MAKE_PIN(PORT_B, 2)
#define OUT_A0_WPORT	PORTB
#define OUT_A0_PIN		2

#define PIN_OUT_A1		DIGITAL_MAKE_PIN(PORT_B, 3)
#define OUT_A1_WPORT	PORTB
#define OUT_A1_PIN		3

#define PIN_SS			DIGITAL_MAKE_PIN(PORT_B, 4)
#define SPI_SS_WPORT	PORTB
#define SPI_SS_PIN		4

#define PIN_MOSI		DIGITAL_MAKE_PIN(PORT_B, 5)
#define PIN_MISO		DIGITAL_MAKE_PIN(PORT_B, 6)
#define PIN_SCK			DIGITAL_MAKE_PIN(PORT_B, 7)

// PORT C
// set high to read buttons, low for LCD
#define LCD_MUX_BTN_LCD_N		DIGITAL_MAKE_PIN(PORT_C, 0)
#define LCD_RW			DIGITAL_MAKE_PIN(PORT_C, 1)
#define LCD_EN			DIGITAL_MAKE_PIN(PORT_C, 2)
#define LCD_RS			DIGITAL_MAKE_PIN(PORT_C, 3)
#define LCD_D4			DIGITAL_MAKE_PIN(PORT_C, 4)
#define LCD_D5			DIGITAL_MAKE_PIN(PORT_C, 5)
#define LCD_D6			DIGITAL_MAKE_PIN(PORT_C, 6)
#define LCD_D7			DIGITAL_MAKE_PIN(PORT_C, 7)
#define BTN_DOWN_LEFT	DIGITAL_MAKE_PIN(PORT_C, 4) // shared with LCD_D4
#define BTN_OK			DIGITAL_MAKE_PIN(PORT_C, 5) // shared with LCD_D5
#define BTN_UP_RIGHT	DIGITAL_MAKE_PIN(PORT_C, 6) // shared with LCD_D6
#define BTN_MENU		DIGITAL_MAKE_PIN(PORT_C, 7) // shared with LCD_D7

// PORT D
#define DIG_B			DIGITAL_MAKE_PIN(PORT_D, 2)
#define DIG_A			DIGITAL_MAKE_PIN(PORT_D, 3)

#define PIN_SPI_CS		DIGITAL_MAKE_PIN(PORT_D, 4)
#define SPI_CS_WPORT	PORTD	
#define SPI_CS_PIN		4
		
#define PIN_LED_B		DIGITAL_MAKE_PIN(PORT_D, 5)
#define LED_B_WPORT		PORTD
#define LED_B_PIN		5

#define PIN_LED_A		DIGITAL_MAKE_PIN(PORT_D, 6)
#define LED_A_WPORT		PORTD
#define LED_A_PIN		6

#define LCD_BL_N		DIGITAL_MAKE_PIN(PORT_D, 7)


// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _PINS_MK312_H

