/*
 * Copyright (c) 2019 zaphod
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


/*
 * Description:
 *
 */

#ifndef _TASK_UI_H
#define _TASK_UI_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdint.h>

#include "def_task.h"

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

typedef struct {
	uint8_t	starting_mode;

	uint8_t	ramp_start;		// 50..100 %
	uint8_t	ramp_time;		// 1..120 sec
	uint8_t	depth;			// 10..100
	uint8_t	tempo;			// 1..100
	uint8_t	frequency;		// Hz
	uint8_t	effect;			// 1..100
	uint8_t	pulse_width;	// 70..250 us
	uint8_t	pace;

	uint8_t	split_mode_enabled;	// 0..1
	uint8_t	split_mode_a;	// 0..num_modes-1
	uint8_t	split_mode_b;	// 0..num_modes-1

	uint8_t	power_level;	// 0..2 = low,normal,high

	uint8_t	pause_at_powerup;

	uint16_t	checksum;
} tSettings;

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// data that is always restored from EEPROM at startup
extern tSettings	settings;

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

void task_ui_init(void);
void task_ui_poll(void);

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _TASK_UI_H

