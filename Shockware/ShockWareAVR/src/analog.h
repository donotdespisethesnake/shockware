/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */


/*
 * Description:
 *
 */
 
#ifndef _ANALOG_H
#define _ANALOG_H

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

extern uint16_t ain_output_current;	// 0-10 A
extern uint16_t ain_setting_f;			// percent
extern uint16_t ain_input_voltage;		// 0-20 V
extern uint16_t ain_batt_voltage;		// 0-20 V
extern uint16_t ain_setting_a;			// percent
extern uint16_t ain_setting_b;			// percent
extern uint16_t ain_audio_a;
extern uint16_t ain_audio_b;

// derived values
extern uint8_t  battery_level;			// 0..6
extern uint8_t  battery_percent;		// 0..100

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

void analog_poll (void);

void dump_analog_values (void);

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

#endif // _ANALOG_H

