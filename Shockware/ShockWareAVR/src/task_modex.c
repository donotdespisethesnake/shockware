/*
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

/*
 * Description:  Task for Module Execution.
 *
 */

// --------------------------------------------------------------------------
// Includes
// --------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "dev_timer.h"
#include "dev_serial.h"
#include "dev_dac_ltc1661.h"
#include "def_board.h"
#include "analog.h"
#include "task_ui.h"
#include "task_modex.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------


typedef enum
{
// Note: these are bit flags
	SM_FIXED		= 1,
	SM_MA_CONTROL	= 2,
	SM_TIMER_244_HZ	= 4,
	SM_TIMER_30_HZ  = 8,
	SM_TIMER_1_HZ	= 0x10
} Source_mode_t;

// 
typedef enum {
// Note: these are bit flags
	AC_NONE				= 0x00,	// aka stop
	AC_REVERSE			= 0x01,	
	AC_LOAD_MODULE		= 0x02,
	AC_OUTPUT_ON		= 0x04,
	AC_OUTPUT_OFF		= 0x08,
	AC_OUTPUT_TOGGLE	= 0x10,
} eAction;

typedef struct {
	eAction  op;
	uint8_t  param;
} Action_t;


typedef struct {
	int16_t         value;
	uint8_t			value_min;
	uint8_t			value_max;
	int8_t			step;

	uint8_t			rate;
	uint16_t		rate_accum;
	uint8_t			rate_min;
	uint8_t			rate_max;

	Source_mode_t	source_select;

	Action_t		min_action;
	Action_t		max_action;

} param_t;

typedef struct
{
	bool			enabled;

	param_t			module_intensity;	// 0-255
	param_t			pulse_width;		// 1-255 us
	param_t			frequency;	   		// 1-255 Hz

	// output gate control
	uint8_t			gate_on;
	uint8_t			gate_off;
	uint8_t			gate_timer;			// counter
	Source_mode_t	gate_source;

	
	Source_mode_t	module_timer_source;
	uint8_t			module_next;
	uint8_t			module_timer_reload;	// enabled if != 0
	uint8_t			module_timer_count;

	uint16_t		effective_intensity;// 0-1023

	// MA parameters
	uint8_t			ma_min;
	uint8_t			ma_max;

	// 
	struct {
		uint8_t		pulse_width;
		uint16_t	timer_val;
		uint16_t	intensity;
	} reload_params;

} ChannelData_t;


typedef uint16_t int_6q10_t;

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------



// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

uint16_t power_setting = 340;

uint8_t	ramp_setting;	// 0..255 
uint8_t	ramp_rate;
uint8_t	ramp_step;

// NB: These names must correspond to eModeId
const char *mode_name [MAX_MODES] = {
	"Waves", 
	"Stroke", 
	"Climb", 
	"Combo", 
	"Intense", 
	"Rhythm",
	"None",
	};



// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

ChannelData_t channel_a;
ChannelData_t channel_b;

uint8_t	cur_mode;

uint8_t	num_modes = 6;

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

void delay_f (volatile uint16_t delay)
{
	delay = delay * 18 / 32 - 6;

	while (delay--)
	{ }
}

void update_output_intensity (void)
{
	// update DACs if value changed

	if (channel_a.reload_params.intensity != channel_a.effective_intensity)
	{
		channel_a.reload_params.intensity = channel_a.effective_intensity;
		dac_write_and_update(DAC_CHANNEL_A, channel_a.reload_params.intensity);
	}

	if (channel_b.reload_params.intensity != channel_b.effective_intensity)
	{
		channel_b.reload_params.intensity = channel_b.effective_intensity;
		dac_write_and_update(DAC_CHANNEL_B, channel_b.effective_intensity);
	}
}


int_6q10_t mul_6q10 (int_6q10_t a, int_6q10_t b)
{
	uint32_t temp = a * b;

	return temp >> 10;
}


// map a value in range in_min..in_max into out_min..out_max
int16_t map(int16_t x, int16_t in_min, int16_t in_max, int16_t out_min, int16_t out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

int16_t invert(int16_t x, int16_t in_min, int16_t in_max)
{
	return in_max - x + in_min;
}


ISR(TIMER1_COMPA_vect)
{
	if (channel_a.enabled)
	{
		CLEAR_PIN (LED_A);
#if 1
		SET_PIN(OUT_A0);
		delay_f (channel_a.pulse_width.value);
		CLEAR_PIN(OUT_A0);

		SET_PIN(OUT_A1);
		delay_f (channel_a.pulse_width.value);
		CLEAR_PIN(OUT_A1);
#endif
		SET_PIN (LED_A);
	}

	if (channel_b.enabled)
	{
		CLEAR_PIN (LED_B);
#if 1
		SET_PIN(OUT_B0);
		delay_f (channel_b.pulse_width.value);
		CLEAR_PIN(OUT_B0);

		SET_PIN(OUT_B1);
		delay_f (channel_b.pulse_width.value);
		CLEAR_PIN(OUT_B1);
#endif
		SET_PIN (LED_B);
	}

	// set output levels?
	update_output_intensity();

	set_timer_frequency (1, channel_a.frequency.value);
}

ISR(TIMER1_COMPB_vect)
{

}

void load_module (ChannelData_t *channel, uint8_t module_number)
{
	switch (module_number)
	{
// --------------------------------------------------------------------------
		case 1:
		// set defaults
		memset (channel, 0 , sizeof (ChannelData_t));

		channel->module_intensity.value = 255;
		channel->module_intensity.value_min = 205;
		channel->module_intensity.value_max = 255;
		channel->module_intensity.step = 1;
		channel->module_intensity.rate = 1;
		channel->module_intensity.rate_accum = 0;
		channel->module_intensity.rate_min = 1;
		channel->module_intensity.rate_max = 100;
		channel->module_intensity.source_select = SM_FIXED;
		channel->module_intensity.min_action.op = AC_NONE;
		channel->module_intensity.max_action.op = AC_NONE;

		channel->frequency.value     = 100;	// 22
		channel->frequency.value_min = 30;	// 9
		channel->frequency.value_max = 250;	// 100
		channel->frequency.step      = 1;
		channel->frequency.rate		 = 1;
		channel->frequency.rate_accum = 0;
		channel->frequency.rate_min = 1;
		channel->frequency.rate_max = 100;
		channel->frequency.source_select = SM_MA_CONTROL;
		channel->frequency.min_action.op = AC_NONE;
		channel->frequency.max_action.op = AC_NONE;

		channel->pulse_width.value	= 50;		// settings.pulse_width;   // 130 if timer
		channel->pulse_width.value_min = 50;
		channel->pulse_width.value_max = 200;
		channel->pulse_width.step  = 1;
		channel->pulse_width.rate  = 1;
		channel->pulse_width.rate_accum = 0;
		channel->pulse_width.rate_min = 1;
		channel->pulse_width.rate_max = 100;
		channel->pulse_width.source_select = SM_FIXED;
		channel->pulse_width.min_action.op = AC_NONE;
		channel->pulse_width.max_action.op = AC_NONE;
	
		channel->gate_on  = 0;
		channel->gate_off = 0;
		channel->gate_timer = 0;
		channel->gate_source = SM_TIMER_244_HZ;

		channel->module_timer_source = SM_FIXED;
		channel->module_next = 0;
		channel->module_timer_reload = 0;
		channel->module_timer_count = 0;

		channel->effective_intensity   = 0;

		channel->ma_min = 1;
		channel->ma_min = 64;
		break;

// --------------------------------------------------------------------------
		case 2: // intense (B)
			channel->gate_on = 64;
			channel->gate_off = 64;
			channel->gate_timer = 0;
			channel->gate_source = SM_TIMER_244_HZ;
		break;

// --------------------------------------------------------------------------
		case 11: // waves (AB)
			channel->frequency.value 		= 100;
			channel->frequency.value_min	= 30;
			channel->frequency.value_max	= 250;
			channel->frequency.step			= 1;
			channel->frequency.rate_min		= 1;
			channel->frequency.rate_max		= 64;
			channel->frequency.source_select = SM_TIMER_244_HZ;
			channel->frequency.min_action.op = AC_REVERSE;
			channel->frequency.max_action.op = AC_REVERSE;
			
			channel->pulse_width.value		= 100;
			channel->pulse_width.value_min	= 50;
			channel->pulse_width.value_max	= 130;
			channel->pulse_width.step		= 2;
			channel->pulse_width.rate_min		= 1;
			channel->pulse_width.rate_max		= 64;
			channel->pulse_width.source_select = SM_TIMER_244_HZ;
			channel->pulse_width.min_action.op = AC_REVERSE;
			channel->pulse_width.max_action.op = AC_REVERSE;

			channel->ma_min = 1;
			channel->ma_min = 64;
		break;

// --------------------------------------------------------------------------
		case 12: // waves (B)
			channel_b.frequency.value_max /= 2;
			channel_b.pulse_width.step = 3;
		break;

// --------------------------------------------------------------------------
		case 14: // intense (AB)
			channel->frequency.value		= 100;
			channel->frequency.value_min	= 15;
			channel->frequency.value_max	= 245;
			channel->frequency.source_select = SM_MA_CONTROL;
		break;

		case 3: // stroke (AB)
			channel->ma_min = 1;
			channel->ma_min = 32;

			channel->module_intensity.source_select = SM_TIMER_244_HZ;
			channel->module_intensity.step = 2;
			channel->module_intensity.rate = 1;
			channel->module_intensity.rate_min = 1;
			channel->module_intensity.rate_max = 32;
			channel->module_intensity.value_min = 0;
			channel->module_intensity.value_max = 255;
			channel->module_intensity.min_action.op = AC_REVERSE | AC_OUTPUT_ON; 
			channel->module_intensity.max_action.op = AC_REVERSE | AC_OUTPUT_OFF;

			channel->frequency.source_select = SM_FIXED;
		break;

		case 4: // stroke (B)
			channel->module_intensity.step = 1;
			channel->module_intensity.value_min = 230;

			channel->pulse_width.value		= 216;
		break;

		case 5: // climb (AB)
			channel->ma_min = 1;
			channel->ma_min = 100;

			channel->frequency.max_action.op = AC_LOAD_MODULE;
			channel->frequency.max_action.param = 6;
		break;

		case 6: // climb (A)
			channel->frequency.step = 2;
			channel->frequency.max_action.op = AC_LOAD_MODULE;
			channel->frequency.max_action.param = 7;
		break;

		case 7: // climb (A)
			channel->frequency.step = 4;
			channel->frequency.max_action.op = AC_LOAD_MODULE;
			channel->frequency.max_action.param = 5;
		break;

		case 8: // climb (B)
			channel->ma_min = 1;
			channel->ma_min = 100;

			channel->frequency.max_action.op = AC_LOAD_MODULE;
			channel->frequency.max_action.param = 9;
		break;

		case 9: // climb (B)
			channel->frequency.step = 2;
			channel->frequency.max_action.op = AC_LOAD_MODULE;
			channel->frequency.max_action.param = 10;
		break;

		case 10: // climb (B)
			channel->frequency.step = 5;
			channel->frequency.max_action.op = AC_LOAD_MODULE;
			channel->frequency.max_action.param = 8;
		break;


	}
}

static void update_gating (ChannelData_t *channel)
{
	// output gating
	if (channel->gate_on)
	{
		if (channel->gate_timer == 0)
		{
			channel->gate_timer = channel->gate_on + channel->gate_off - 1;
		}
		if (channel->gate_timer < channel->gate_on)
			channel->enabled = true;
		else
			channel->enabled = false;
		channel->gate_timer--;
	}
}

void perform_action (ChannelData_t *pchannel, Action_t action)
{
	if (action.op & AC_OUTPUT_OFF)
		pchannel->enabled = false;

	if (action.op & AC_OUTPUT_ON)
		pchannel->enabled = true;

	if (action.op & AC_OUTPUT_TOGGLE)
		pchannel->enabled = !pchannel->enabled;

	if (action.op & AC_LOAD_MODULE)
		load_module (pchannel, action.param);
}

void update_param (ChannelData_t *pchannel, param_t *param, Source_mode_t events)
{
	uint8_t  steps;
	uint16_t range;

	if (events & param->source_select)
	{
		steps = 0;
		// todo: timer source?
		// MA controls the rate
		if (events & param->source_select & (SM_TIMER_244_HZ | SM_TIMER_30_HZ | SM_TIMER_1_HZ))
		{
			param->rate = map (ain_setting_f, 1, 100, param->rate_min, param->rate_max);
			param->rate = invert (param->rate, param->rate_min, param->rate_max ) ;
			param->rate_accum += 256 / param->rate;

			if (param->rate_accum > 255)
			{
				steps = param->rate_accum >> 8;
				param->rate_accum = param->rate_accum & 0x00FF;
			}
		}

		switch (param->source_select)
		{
			case SM_FIXED:
			default:
			{
			}
			break;

			case SM_MA_CONTROL:
			{
				range = param->value_max - param->value_min;
				param->value = ain_setting_f * range / 100 + param->value_min;
			}
			break;

			case SM_TIMER_244_HZ:
			case SM_TIMER_30_HZ:
			case SM_TIMER_1_HZ:
			{
				if (steps > 0)
				{
					if (param->step > 0)
					{
						if (param->value + param->step >= param->value_max)
						{
							// reverse?
							if (param->max_action.op & AC_REVERSE)
								param->step = -param->step;

							perform_action(pchannel, param->max_action);
						}
						else
							param->value += param->step;
					}
					else
					{
						if (param->value + param->step <= param->value_min)
						{
							// reverse?
							if (param->min_action.op & AC_REVERSE)
								param->step = -param->step;

							perform_action(pchannel, param->min_action);
						}
						else
							param->value += param->step;
					}
				}
			}
			break;
		}
	}
}


void update_channel (uint8_t channel_no, Source_mode_t events)
{
	uint32_t temp;
	ChannelData_t *channel;

	if (channel_no == 0)
		channel = &channel_a;
	else
		channel = &channel_b;

	// --------------------------------------------------------------------------
	// set intensity
	update_param (channel, &channel->module_intensity, events);

	if (channel_no == 0)
		temp = (uint32_t)ain_setting_a;
	else
		temp = (uint32_t)ain_setting_b;

	temp = (uint32_t)power_setting * temp / 100L;
	temp = temp * (uint32_t)channel->module_intensity.value / 255L;
	temp = temp * (uint32_t)ramp_setting / 255L;

	channel->effective_intensity = temp;

	// --------------------------------------------------------------------------
	// set pulse width
	update_param (channel, &channel->pulse_width, events);

	// --------------------------------------------------------------------------
	// output gating
	if (events & channel->gate_source)
	{
		update_gating(channel);
	}

	// --------------------------------------------------------------------------
	// set frequency
	update_param (channel, &channel->frequency, events);
}

/*
	This is called at approx. 488.3 Hz, assuming 8 MHz/(64*256)
*/
void modex_timer_cb (void)
{
	static uint16_t ticks=0;
	Source_mode_t timer_events = SM_MA_CONTROL;

	ticks++;

	
	if ((ticks & 1) == 0)
	{
		timer_events |= SM_TIMER_244_HZ;
		// divide by 2 = 244.1 Hz timer
	}

	if ((ticks & 15) ==0)
	{
		// divide by 16 = 30.5 Hz timer
		timer_events |= SM_TIMER_30_HZ;

		if (ramp_setting < 255)
			ramp_setting += ramp_step;
			// todo: need to update outputs ?
	}

	if (ticks == 512)
	{
		// divide by 512 = 0.953 Hz timer
		timer_events |= SM_TIMER_1_HZ;

		ticks = 0;
	}

	update_channel (0, timer_events);
	update_channel (1, timer_events);
}

// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------

void set_mode (uint8_t mode)
{
	cur_mode = mode;

 	channel_a.enabled = false;
 	channel_b.enabled = false;

	ramp_setting = 255 * 0.6f;
	ramp_rate = 7;
	ramp_step = 1;

	load_module(&channel_a, 1);
	channel_b = channel_a;

	//
	switch (mode)
	{
		case MODE_WAVES:
			load_module(&channel_a, 11);

			if (!settings.split_mode_enabled)
			{
				channel_b = channel_a;
				load_module(&channel_b, 12);
			}
		break;

		case MODE_INTENSE:
			load_module(&channel_a, 14);

			if (!settings.split_mode_enabled)
			{
				channel_b = channel_a;
				load_module(&channel_b, 2);
			}
			break;

		case MODE_STROKE:
			load_module(&channel_a, 3);

			if (!settings.split_mode_enabled)
			{
				channel_b = channel_a;
				load_module(&channel_b, 4);
			}
			break;

		case MODE_CLIMB:
			load_module(&channel_a, 5);

			if (!settings.split_mode_enabled)
			{
				channel_b = channel_a;
				load_module(&channel_b, 8);
			}
			break;

		case MODE_COMBO:
			load_module(&channel_a, 13);

			if (!settings.split_mode_enabled)
			{
				channel_b = channel_a;
				load_module(&channel_b, 33);
			}
			break;

		case MODE_RHYTHM:
			load_module(&channel_a, 15);

			if (!settings.split_mode_enabled)
			{
				channel_b = channel_a;
			}
			break;
	}

 	channel_a.enabled = true;
 	channel_b.enabled = true;
}

/** @brief
 *  @param[in]
 *  @param[out]
 *  @return
 */

void task_modex_init(void)
{
	channel_a.enabled = channel_b.enabled = false;

	channel_a.frequency.value = 100;

	set_timer_frequency (1, channel_a.frequency.value);
}


char buf [16];

void task_modex_poll(void)
{
	//sprintf (buf, "%d %d\n", channel_a.frequency, channel_a.frequency_step);
	//serial_send_str (SERIAL_0, buf);

}


// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------

