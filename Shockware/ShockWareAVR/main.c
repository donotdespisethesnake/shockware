/*
 * ShockWare
 *
 * Copyright (c) 2019 zaphod
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 */

 // --------------------------------------------------------------------------
 // Includes
 // --------------------------------------------------------------------------

#include <stdlib.h>

#include <avr/io.h>
#include <util/atomic.h>
#include <util/delay.h>
//#include <interrupt.h>

#include "dev_gpio.h"
#include "dev_serial.h"
#include "dev_timer.h"
#include "dev_delay.h"

#include "dev_lcd_char.h"
#include "dev_button.h"
#include "dev_dac_ltc1661.h"

#include "def_board.h"

#include "task_ui.h"
#include "task_serial_if.h"
#include "task_modex.h"

// --------------------------------------------------------------------------
// Externals
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Local defines
// --------------------------------------------------------------------------

#define cpu_irq_enable()  sei()

// --------------------------------------------------------------------------
// Types
// --------------------------------------------------------------------------

typedef struct {
	task_func_t init;
	task_func_t poll;
} task_def_t;


// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Public Variables
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private Variables
// --------------------------------------------------------------------------

task_def_t tasks [] = {
	{task_ui_init, task_ui_poll},
	{task_modex_init, task_modex_poll},
	{task_serial_if_init, task_serial_if_poll}
};

#define NUM_TASKS sizeof(tasks)/sizeof(tasks[0])

//#define NUM_TASKS 3

// --------------------------------------------------------------------------
// Function prototypes
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Private functions
// --------------------------------------------------------------------------

#if 0
void timer1_cb (void)
{
	static uint8_t timer_30Hz=0;

	timer_30Hz++;

	if ((timer_30Hz & 3) == 0)
	{
		// 244.1 Hz timer
	}

	if (timer_30Hz == 64)
	{
		// 30.5 Hz timer
		timer_30Hz = 0;
	}
}
#endif



// --------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------


int main (void)
{
	uint8_t j;

	/* System initialization. */

	bdk_board_init();

	/* Application code. */

	dac_init();
	dac_write_and_update(DAC_CHANNEL_A, 0);
	dac_write_and_update(DAC_CHANNEL_B, 0);

	set_pin (PIN_LED_A);
	set_pin (PIN_LED_B);

	init_timer (0, modex_timer_cb, 1000);
	init_timer (10, NULL, 1000);
	//init_timer (1, NULL, 1000);


	for (j=0; j < NUM_TASKS; j++)
		tasks[j].init();

	cpu_irq_enable();

	while (1)
	{
		for (j=0; j < NUM_TASKS; j++)
			tasks[j].poll();
	}

}


